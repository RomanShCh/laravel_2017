<?php

use Illuminate\Database\Seeder,
	 App\Models\Role;	

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Role::create([
				'name_role'=>'admins',
				'is_create'=>1,
				'is_update'=>1,
				'is_delete'=>1,
			]);
			
		Role::create([
				'name_role'=>'contentUsers',
				'is_create'=>1,
				'is_update'=>1,
				'is_delete'=>0,
			]);
		
		Role::create([
				'name_role'=>'users',
				'is_create'=>0,
				'is_update'=>0,
				'is_delete'=>0,
			]);		
    }
}
