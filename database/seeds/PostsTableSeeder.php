<?php

use Illuminate\Database\Seeder,
    App\Models\Post;


class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
   {
      $faker = Faker\Factory::create('ru_RU'); // create a Russian faker
		$first=0; $last=0;
		
		for($i=0; $i<10; $i++)
		{
			Post::create([
				'preview'=>$faker->realText(300),
				'post'=>$faker->realText(1024),
				'author'=>'author',
				'post_title'=>$faker->realText(50),
				'image'=> $faker->imageUrl(640, 480),
				'user_id'=>'2',
			]);
			if ($i==0){
				$first=Post::first()->id;
				$last=$first+9;
			}
			
		}	
			$favorite=Post::find(mt_rand($first, $last));
			$favorite->is_favorite=1;
			$favorite->save();
		 
		 
   }
}
