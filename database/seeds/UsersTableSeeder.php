<?php

use Illuminate\Database\Seeder,
	 App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
				'name'=>'admin',
				'email'=>'admin@comp.com',
				'password'=>bcrypt(123456),
				'phone'=>'+7 (123) 123-45-78',
				'role_id'=>1,
		  ]);
		  
		  User::create([
				'name'=>'editor',
				'email'=>'editor@comp.com',
				'password'=>bcrypt(123456),
				'phone'=>'+7 (123) 123-45-78',
				'role_id'=>2,
		  ]);
		  
		   User::create([
				'name'=>'user',
				'email'=>'user@comp.com',
				'password'=>bcrypt(123456),
				'phone'=>'+7 (123) 123-45-78',
				'role_id'=>3,
		  ]);
    }
}
