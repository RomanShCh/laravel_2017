<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddedColumsTitlepostIsActiveOther extends Migration
{
	 /**
	  * Run the migrations.
	  *
	  * @return void
	  */
	 public function up()
	 {
		Schema::table('posts', function (Blueprint $table) {
			$table->text('preview')->nullable()->change();
			$table->text('post')->nullable()->change();
			$table->string('author')->nullable()->change();
			$table->string('post_title', 255);
			$table->string('image',255)->nullable();
			$table->string('tagline',255)->nullable();
			$table->boolean('is_active')->default(1);
			$table->timestamp('active_from')->nullable();
         $table->timestamp('active_to')->nullable();
			$table->softDeletes();
			
		});
	 }

	 /**
	  * Reverse the migrations.
	  *
	  * @return void
	  */
	 public function down()
	 {
		Schema::table('posts', function (Blueprint $table) {
		  $table->dropColumn([
					'post_title', 'image', 'tagline',
					'is_active', 'active_from', 'active_to',
					'deleted_at',
				]);
		});
	 }
}
