<?php
namespace App\Http\Controllers\NewsBlog;

use Illuminate\Http\Request;
use App\Http\Controllers\Blog; 
use App\Models\Post, App\Models\Comment, App\Models\User;
use App\Http\Requests\ValidBlog;
use Illuminate\Support\Facades\Auth;


class BlogNews extends Blog
{
	
	public function index()
	{
		$posts=Post::with('user')->withCount('comments')->paginate(2);
		if($posts->currentPage() > $posts->lastPage())
		{
			abort(404);
		}
		 
		$favPosts=Post::favorite(); /*added scopeFavorite*/
				
		return view('layouts.tmpltForTwo', [
						'page'=>'frontend.main',
						'title'=>'Главная',
						'posts' =>$posts,
						'favPosts'=>$favPosts,
						
				]);
				
	}
	
	public function showOne($id)
	{
		$post=Post::withCount('comments')/*added virtual colum {relation}_count*/
					->with('comments','comments.user')
					->findOrFail($id);
		
		return view('layouts.columOne', [
					'page'=>'frontend.article',
					'title'=>'Пост',
					'item'=>$post,
		]);
	}
	
	public function addComment(ValidBlog $request, $id)
	{
		/*  $this->validate($request, [
			'comment'=>'required',
      ]); */
		
		$post=Post::findOrFail($id);
		$post->comments()
			  ->create([
					'comment'=>$request->input('comment'),
					'user_id'=>$request->user()->id,
				]);
			
		return redirect()
				->route('blog.news.showone', ['id'=>$id]);
	}
	
	public function addPostGet()
	{
		$this->authorize('create', Post::class);
		
		
		return view('layouts.columOne', [
					'page'=>'frontend.addArticle',
					'title'=>'Добавление поста',
		]);
		
	}
	
	public function addPost(ValidBlog $request)
	{
		$this->authorize('create', Post::class);
		
		/* $this->validate($request, [
			'titlePost'=>'required|max:255',
			'post'=> 'required',
      ]); */
		
		/* $faker = Faker\Factory::create('ru_RU'); // create a Russian faker */
		
		Post::create([
			'is_active'=>1,
			'user_id'=> $request->user()->id,
			'post_title'=>$request->input('titlePost'),
			'post'=>$request->input('post'),
			/* 'image'=> $faker->imageUrl(640, 480), */
			'author'=>$request->user()->name,
		]);
		
		return redirect()
				->route('blog.news.index');
	}
	
	public function editPostGet(Post $post, $id)
	{
		$post=Post::findOrFail($id);
		
		$this->authorize('update', $post);
		
		return view('layouts.columOne', [
					'page'=>'frontend.editArticle',
					'title'=>'Редактирование поста',
					'post'=>$post,
		]);
	}
	
	public function editPost(ValidBlog $request, $id)
	{
		$post=Post::findOrFail($id);
		$this->authorize('update', $post);
		
		/* $this->validate($request, [
			'post_title'=>'required|max:255',
			'post'=> 'required',
      ]); */
		
		$post->fill($request->all());
		$post->save();
		
		return back();
	}
	
	public function delPost($id)
	{
		$post=Post::findOrFail($id);
		
		$this->authorize('delete', $post);
		$post->delete();
		
		return redirect()
				->route('blog.news.index');
	}
}

























