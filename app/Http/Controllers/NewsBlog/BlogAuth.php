<?php
	namespace App\Http\Controllers\NewsBlog;

	use Carbon\Carbon;
	use Illuminate\Http\Request;
	use App\Http\Controllers\Blog;
	use App\Http\Requests\ValidBlog;
	use Illuminate\Support\Facades\Auth;
	use Illuminate\Support\Facades\DB;
	use App\Models\User,
		  App\Models\Role;

	class BlogAuth extends Blog
	{
		/*
		* Методы регистрации и аутен-ции для пользователей
		* обработка post и get запросов
 		*/
		public function loginGet()
		{
			return view('layouts.columOne', [
							'page'=>'frontend.loginGet',
							'title'=>'Вход'
					]);
		}
		public function loginPost(Request $request)
		{
			$remember = ($request->has('remember')) ? true : false; 
			$checkAuth = Auth::attempt([
								'email' => $request->input('email'), 
								'password' => $request->input('pass'),
							],	$remember);
			
			if ($checkAuth) 
			{
				return redirect()
					->route('blog.news.index');
			}
			else 
			{
				return redirect()
					->route('blog.auth.loginGet')
					->with('authErr', trans('custom.wrongAuth'));
			}
			
		}
		
		public function registrGet()
		{
			return view('layouts.columOne', [
							'page'=>'frontend.registrGet',
							'title'=>'Регистрация'
					]);
		}
		public function registrPost(ValidBlog $request)
		{
			User::create([
				'name'=>$request->input('user'),
				'email'=>$request->input('email'),
				'password'=>bcrypt($request->input('pass')),
				'phone'=>$request->input('tel'),
				'role_id'=>Role::where('name_role', 'users')
								->firstOrFail()->id,
			]);
					
			return redirect()
				->route('blog.news.index');
		}
		
		public function logout()
		{
			Auth::logout();
			return back();
			
		}
		
	}

















