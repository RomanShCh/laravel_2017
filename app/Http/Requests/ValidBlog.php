<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Router;

class ValidBlog extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *|unique:users
     * @return array
     */
    public function rules()
    {
			/* debug(Route::currentRouteNamed('blog.news.addPost')); */
			/* debug($this->route()->getName());
			return 'ok'; */
			
			if($this->route()->getName()=='blog.auth.registrPost')
			{
				return [
						/*add user*/
						'user'=>'required|max:255',
						'email'=>'bail|required|email|unique:users',
						'pass'=>'required|max:255|min:6',
						'pass1'=>'required|same:pass',
						'tel'=>'regex:/\+\d{1}\s{1}\(\d{3}\)\s{1}\d{3}\-\d{2}\-\d{2}/',
						'comfirm'=>'accepted',
				];
			}
			elseif($this->route()->getName()=='blog.news.addComm')
			{
				return[
					'comment'=>'required',
				];
			}
			
			elseif($this->route()->getName()=='blog.news.addPost')
			{
				return[
					'titlePost'=>'required|max:255',
					'post'=> 'required',
				];
			}
			
			elseif($this->route()->getName()=='blog.news.editPost')
			{
				return[
					'post_title'=>'required|max:255',
					'post'=> 'required',
				];
			}
				
		
    }
}
