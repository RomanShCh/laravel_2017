<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Model
{
   use SoftDeletes;
	
		protected $guarded=['id', 'created_at', 'updated_at', 'deleted_at'];
		protected $hidden=['password'];
		protected $table = 'users'; 
		protected $dates = ['deleted_at'];
		
	public function comment()
   {
      return $this->hasMany('App\Models\Comment');
   }
	
	/* public function role()
   {
      return $this->belongsTo('App\Models\Role');
   } */
	
	public function post()
   {
      return $this->hasMany('App\Models\Post');
   }
  
}
