<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
   use SoftDeletes;

		protected $guarded=['id', 'created_at', 'updated_at', 'deleted_at'];
		/* protected $hidden=['password']; */
		protected $table = 'posts'; 
		protected $dates = ['deleted_at'];
		
	/**
     * scope a query to favorite posts.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
	 public function scopeFavorite($query)
	{
		return $query->where('is_favorite', 1)
						 ->take(4)
						 ->get();
	}
	
	
	public function comments()
   {
      return $this->hasMany('App\Models\Comment');
   }
	
	public function user()
   {
      return $this->belongsTo('App\Models\User');
		
   }
	
	
	
	
}
