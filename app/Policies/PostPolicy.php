<?php

namespace App\Policies;

use App\Models\User, App\Models\Post;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Auth;

class PostPolicy
{
    use HandlesAuthorization;
	
   public function before($user)/*filter policy*/
   {
		if ($user->role->name_role == 'admins'){
			return true;
		}
		if ($user->role->name_role == 'users'){
			return false;
		}
	}
	   
   public function create($user)
   {
      return $user->role->name_role == 'contentUsers';
   }

   
   public function update($user, $post)
   {
      return $user->role->name_role == 'contentUsers'
					&& $user->id == $post->user_id;
   }

    
   public function delete($user, Post $post)
   {
      return false;
   }
}
