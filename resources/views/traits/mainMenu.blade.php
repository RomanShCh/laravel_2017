	     <!-- Logo -->
			<div id="logo">
				<a href="{{ route('blog.news.index') }}"></a>
			</div>

			<!-- Main Menu -->
			<div id="menu">
				<a class="mh" href="{{ route('blog.news.index') }}">Домашняя</a>
				<a class="mt" href="{{ route('blog.auth.registrGet') }}">Регистрация</a>
				@can('create', App\Models\Post::class)
				<a class="mw" href="{{ route('blog.news.addPostGet') }}">Добавить<br>статью</a>
				@endcan
			</div>