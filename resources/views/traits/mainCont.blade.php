
@forelse ($posts as $post)
	<!-- Post -->
	<div class="post">
		
				
			<!-- Post Details -->
			<div class="post_inf">
				<span>{{$post->created_at}}</span><br />
				<span class="posn">{{$post->user->name}}</span><br />
				<a class="posc" href="{{ route('blog.news.showone',['id' => $post->id]) }}">
					коментарий {{$post->comments_count}}
				</a>
			</div>

			<!-- Post Title - Permalink -->
			<h1>
				<a href="{{ route('blog.news.showone',['id' => $post->id] ) }}">{{$post->post_title}}</a>
			</h1>
			
			<!-- Post Content -->
			<a class="pic_lnk" href="{{ route('blog.news.showone',['id' => $post->id] ) }}">
				<img height="300" src="{{$post->image}}" alt="post_pic{{$post->id}}" />
			</a>
			
			<p>{{$post->post}}</p>
			<p>
				@can('update', $post)
				<a class="link_auth" href="{{ route('blog.news.editPostGet',['id' => $post->id] ) }}">
					Редактировать статью
				</a>
				@endcan
				@can('delete', $post)
				<a class="link_auth"  href="{{ route('blog.news.delPost',['id' => $post->id] ) }}">
					Удалить статью
				</a>
				@endcan
			</p>
	</div>
@empty
    <h3><p class="post">Нет доступных постов для отображеня</p></h3>
@endforelse
