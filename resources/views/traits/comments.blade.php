<div class="comments">
	
	@forelse ($item->comments as $comment)
	
	@if(!$comment->is_valid)
	<!-- Comment February 20, 2009 04:33-->
		<div class="comment">

			<div class="comm_hdr">
				<p>{{ $comment->user->name}} <span> | {{ $comment->created_at}}</span></p>
			</div>

			<div class="avat">
				<img src="../../images/avatar.jpg" alt="avatar" />
			</div>
		
			<p class="comm_txt">
			{{$comment->comment}}
			</p>

		</div>
	@endif
	@empty
		<p class="comm_txt">Нет коментарий для отображения</p>
	@endforelse

</div>
