<!-- Footer Menu -->
<div id="foo_menu">

	<a href="{{ route('blog.news.index') }}">Домашняя</a><span>.</span>
	<a href="{{ route('blog.auth.registrGet') }}">Регистрация</a><span>.</span>
	@can('create', App\Models\Post::class)
	<a href="{{ route('blog.news.addPostGet') }}">Добавить статью</a><span>.</span>
	@endcan
	<a href="#">О нас</a><span>.</span>
	<a href="#">Контакты</a>
	
</div>