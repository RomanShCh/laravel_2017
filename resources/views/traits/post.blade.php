<!-- Post -->
	<div class="post">

		<!-- Post Details -->
		<div class="post_inf">

			<span>{{$item->created_at}}</span><br />
			<span class="posn">{{$item->user->name}}</span><br />
			<span class="posc">
				{{$item->comments_count}} Comments
			</span>

		</div>

		<!-- Post Title - Permalink -->
		<h1>
			<a href="#">{{$item->post_title}}</a>
		</h1>
		
		<a class="pic_lnk" href="#">
				<img src="{{$item->image}}" alt="post_pic{{$item->id}}" />
		</a>
		
		<!-- Post Content -->
		<p>
			{{$item->post}}
		</p>
		<p>
				@can('update', $item)
				<a class="link_auth" href="{{ route('blog.news.editPostGet',['id' => $item->id] ) }}">
					Редактировать статью
				</a>
				@endcan
				@can('delete', $item)
				<a class="link_auth"  href="{{ route('blog.news.delPost',['id' => $item->id] ) }}">
					Удалить статью
				</a>
				@endcan
			</p>

	</div>