	<!-- Top Menu -->
	<div id="hdl">
		<a href="#" class="hl">О нас</a>
		<a href="#" class="hl">Контакты</a>
		@if(Auth::check())
			<a href="{{ route('blog.auth.logout') }}" class="hl">
				Выход ({{ Auth::user()->name }})
			</a>
		@else
			<a href="{{ route('blog.auth.loginGet') }}" class="hl">Вход</a>
		@endif
		
	</div>