	@extends('layouts.base')
	
	@section('header')
		<div id="hdr">
			@include('traits.topMenu')
			@include('forms.searchMenu')
			@include('traits.mainMenu')
		</div>
	@endsection
	
	@section('content')
		<div id="cnt_one">
			@include($page)
		</div>
	@endsection
	
	@section('footer')
		<div id="foo">
		<br style="clear: both;"/>
		<br>
			<!-- Footer Container -->
			<div id="foo_d">	
				@include('traits.footerMenu')
				@include('traits.copyright')
			</div>
		</div>
	@endsection