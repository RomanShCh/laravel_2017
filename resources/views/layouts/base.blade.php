<!DOCTYPE>
<html lang="{{ config('app.locale') }}">
	<head>
	   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	   <meta content="true" name="mssmarttagspreventparsing" />
	   <meta http-equiv="imagetoolbar" content="no" />
	   <title>{{$title or '' }}</title>
	   <link href="../../css/style.css" rel="stylesheet" type="text/css" />
		@yield('styleHead')
	   
		<script>
	  /*
		* Simple JS function for setting the search value
		*/
		  function clS(t)
			{
				var srch = document.getElementById('s'), val = srch.value.toString().toLowerCase(), re = /^\s+$/;
				if(t) {
					if(val == 'Search' || val == 'search'){
							  srch.value = '';
					}
				} else {
					if(val == 'Search' || val == 'search' || val == '' || re.test(val)) {
							  srch.value = 'Search';
					}
				}
		   }
		   @yield('scriptHead')
	   </script>


	</head>
	
	<body>
	
	@section('wrapper')
		<div id="wrap">
		@yield('header')
		@yield('content')
		</div>
	@show
		
	@yield('footer')
	
	@yield('bottonScript')
	
	</body>