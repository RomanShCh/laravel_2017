	@extends('layouts.columTwo')
	
	@section('leftCont')
		<!-- Left Container -->
		<div id="lcnt">
			@include($page)
		</div> 
	@endsection
	
	
	@section('rightCont')
		<!-- Right Container -->
		<div id="rcnt">
			@include('widgets.rssTwitt')
			@include('widgets.sponsAds')
			@include('widgets.bestWllpaps')
			@include('widgets.bestPosts')
		</div> 
	@endsection