	<h2>Регистрация</h2>
				
	<form class="cmnt_frm" method="post" action="">
		{{ csrf_field() }}
		<p>
			<input type="text" name="user" value="{{ old('user') }}" size="22" tabindex="1" placeholder="Петр Иванов" required />
			<label>(*)ФИО</label>
		</p>
			<p class="errors">
					@if ($errors->has('user')) 
						 {{ $errors->first('user') }}
					@endif
			</p>		
		<p>
			<input type="email" name="email" value="{{ old('email') }}" size="22" tabindex="2" placeholder="электронный адрес" required />
			<label>(*)(не будет публиковаться)</label>
		</p>
			<p class="errors">
					@if ($errors->has('email')) 
						 {{ $errors->first('email') }}
					@endif
			</p>	
		<p>
			<input type="password" name="pass" size="22" tabindex="3" placeholder="пароль" required />
			<label>(*)</label>
		</p>
			<p class="errors">
					@if ($errors->has('pass')) 
						 {{ $errors->first('pass') }}
					@endif
			</p>	
		<p>
			<input type="password" name="pass1" size="22" tabindex="4" placeholder="подтвердите пароль" required />
			<label>(*)</label>
		</p>
			<p class="errors">
					@if ($errors->has('pass1')) 
						 {{ $errors->first('pass1') }}
					@endif
			</p>
		<p>
			<input type="tel" name="tel" value="{{ old('tel') }}" pattern="+[0-9]{1} ([0-9]{3}) [0-9]{3}-[0-9]{2}-[0-9]{2}" 
					 placeholder="+x (xxx) xxx-xx-xx" size="22" tabindex="5" required />
			<label>(*)тел.</label>
		</p>
			<p class="errors">
					@if ($errors->has('tel')) 
						 {{ $errors->first('tel') }}
					@endif
			</p>
		<p>
			<label class="check">
				<input type="checkbox" name="comfirm" class="checkd" tabindex="6" >Вы согласны с условиями блога
			</label>
		</p>
			<p class="errors">
					@if ($errors->has('comfirm'))
						{{ $errors->first('comfirm') }}
					@endif
			</p>
		<p>
			<input type="submit" name="submit" value="Регистрация" tabindex="7" class="submit"/>
		</p>
	</form>