	<h2>Post your comments</h2>
						
	<!-- Comment Form -->			
	<form class="cmnt_frm" method="post" action="#">
		{{ csrf_field() }}
		<p>Заголовок статьи</p>
			<p>
				<input type="text" name="post_title" size="22" tabindex="1" 
						 id="author"  placeholder="Заголовок статьи"
						 value="{{old('titlePost', $post->post_title)}}"
				/>
			</p>
			<p class="errors">
					@if ($errors->has('post_title')) 
						 {{ $errors->first('post_title') }}
					@endif
			</p>
			<p>Автор статьи</p>
			<p>
				<input type="text" name="author" size="22" 
						value="{{ Auth::user()->name }}" id="email" readonly />
			</p>
			<p>Текст статьи:</p>
			<p>
				<textarea name="post" cols="65" rows="10" tabindex="2" id="comment" >
				{{old('post', $post->post)}}
				</textarea>
			</p>
			<p class="errors">
					@if ($errors->has('post')) 
						 {{ $errors->first('post') }}
					@endif
			</p>
			
			<p>
				<input type="submit"  value="Редактировать статью" tabindex="3" class="submit"/>
			</p>	
	</form>