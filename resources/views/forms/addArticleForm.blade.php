	<h2>Добавление статьи</h2>
						
	<!-- Comment Form -->			
	<form class="cmnt_frm" method="post" >
		{{ csrf_field() }}
			<p>Заголовок статьи</p>
			<p>
				<input type="text" name="titlePost" size="22" tabindex="1" 
						 id="author"  placeholder="Заголовок статьи"/>
			</p>
			<p class="errors">
					@if ($errors->has('titlePost')) 
						 {{ $errors->first('titlePost') }}
					@endif
			</p>
			<p>Автор статьи</p>
			<p>
				<input type="text" name="author" size="22" 
						value="{{ Auth::user()->name }}" id="email" readonly />
			</p>
			<p>Текст статьи:</p>
			<p>
				<textarea name="post" cols="65" rows="10" tabindex="2" id="comment" ></textarea>
			</p>
			<p class="errors">
					@if ($errors->has('post')) 
						 {{ $errors->first('post') }}
					@endif
			</p>
			
			<p>
				<input type="submit" name="submit" value="Добавить статью" tabindex="3" class="submit"/>
			</p>
			
	</form>