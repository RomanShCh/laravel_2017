	<h2>Post your comments</h2>
						
	<!-- Comment Form -->			
	<form class="cmnt_frm" method="post" >
		{{ csrf_field() }}
		@if(Auth::check())
			<p>
				<input type="text" name="author" size="22" tabindex="1" 
						 value="{{ Auth::user()->name }}" id="author" readonly />
			</p>
			<p>
				<input type="text" name="email" size="22" tabindex="2" 
						value="{{ Auth::user()->email }}" id="email" readonly />
			</p>
			<p>
				<textarea name="comment" cols="65" rows="10" tabindex="4" id="comment"></textarea>
			 </p>
			
			<p>
				<input type="submit" name="submit" value="Post comment" tabindex="5" class="submit"/>
			</p>
			<p class="errors">
					@if ($errors->has('comment')) 
						 {{ $errors->first('comment') }}
					@endif
			</p>
		@else 
			<p>
				<span class="errors">Только зарегистрированныйе пользователи могут оставлять комментарии</span>
			</p>
		@endif
	</form>