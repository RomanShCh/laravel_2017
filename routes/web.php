<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
 /*
 *------------FrontEnd Router
 */
	Route::group(['namespace' => 'NewsBlog'], function () 
	{
		Route::get('/', 'BlogNews@index')
			->name('blog.news.index');
		Route::group(['prefix' => 'news'], function () 
		{
			Route::get('one/{id}', 'BlogNews@showOne')
				->name('blog.news.showone');
			Route::post('one/{id}', 'BlogNews@addComment')
				->name('blog.news.addComm');
			
			/*---------For users was authentication*-------------*/
			Route::group(['middleware'=>'auth'], function()
			{
				Route::get('add', 'BlogNews@addPostGet')
					->name('blog.news.addPostGet');
				Route::post('add', 'BlogNews@addPost')
					->name('blog.news.addPost');
				Route::get('edit/{id}', 'BlogNews@editPostGet')
					->name('blog.news.editPostGet');
				Route::post('edit/{id}', 'BlogNews@editPost')
					->name('blog.news.editPost');
				Route::get('delete/{id}', 'BlogNews@delPost')
					->name('blog.news.delPost');
				
			});
		});
		
		/*------For user authentication-------------*/
		Route::group(['prefix' => 'auth'], function ()
		{
			Route::get('login', 'BlogAuth@loginGet')
				->name('blog.auth.loginGet');
			Route::post('login', 'BlogAuth@loginPost')
				->name('blog.auth.loginPost');
			Route::get('registr', 'BlogAuth@registrGet')
				->name('blog.auth.registrGet');
			Route::post('registr', 'BlogAuth@registrPost')
				->name('blog.auth.registrPost');
			Route::get('logout', 'BlogAuth@logout')
				->name('blog.auth.logout');
		});
			
	});
/*
*------------------Admin Routes
*/
	Route::group(['namespace' => 'AdminBlog', 'prefix'=>'admin'], function () 
	{
		Route::get('/', 'AdminAuth@index');
		Route::post('/', 'AdminAuth@auth');
	});	 
	 
	 
	 
	 
	 
	 